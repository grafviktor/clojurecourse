package ru.me.forfun.nodesapp;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.junit.Test;

public class NodeTreeTest {

	@Test
	public void nodeTreeLoadTest() {
		System.out.println("* NodeTree: nodeTreeLoadTest()");

		Random rnd = new Random(System.currentTimeMillis());
		int elements = 10000; //rnd.nextInt(10000);
		List<String> rndStr = new ArrayList<>();
		Deque<Character> stack = new LinkedList<>();
		rndStr.add("[");
		for (int i = 0; i < elements; ++i) {
			int n = rnd.nextInt(3);
			switch (n) {
				case 1:
					stack.add('[');
					rndStr.add("[");
					break;
				case 2:
					if (!stack.isEmpty()) {
						stack.pop();
						rndStr.add("]");
					}
					break;
				default:
					rndStr.add("" + rnd.nextInt(10));
			}
		}

		while (!stack.isEmpty()) {
			stack.pop();
			rndStr.add("]");
		}

		rndStr.add("]");

		String[] strArr = rndStr.toArray(new String[0]);
		NodeTree nt = new NodeTree(strArr);

//		printTree(nt.getRoot());
	}

	private static void printTree(Node root) {
		int sum = 0;
		for (int i : root.getIntegers()) {
			sum += i;
		}
		System.out.printf("%s, sum: %d\n", root, sum);

		for (Node node : root.getChildren()) {
			printTree(node);
		}
	}
}
