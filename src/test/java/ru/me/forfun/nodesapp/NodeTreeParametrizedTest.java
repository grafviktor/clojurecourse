package ru.me.forfun.nodesapp;

import java.util.List;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class NodeTreeParametrizedTest {

	String[] malformedArr;

	public NodeTreeParametrizedTest(String[] malformedArr) {
		this.malformedArr = malformedArr;
	}

	@Parameterized.Parameters
	public static List<Object[]> genParams() {
		Object[][] params = new Object[][]{
			// redundant bracket
			{ new String[] {"[", "1", "[", "2", "3", "]", "4", "[", "5",
				"[", "6", "7", "]", "]", "[", "8", "]", "]", "]"}},

			// a letter in the array
			{ new String[] {"[", "1", "[", "2", "3", "]", "4", "[", "5",
				"[", "6", "z", "]", "]", "[", "8", "]", "]"}},

			// missing the leading bracket
			{ new String[] {"1", "[", "2", "3", "]", "4", "[", "5", "[",
				"6", "7", "]", "]", "[", "8", "]", "]"}}
		};

		return Arrays.asList(params);
	}

	@Test(expected = ExceptionInInitializerError.class)
	public void inputTest() {
		System.out.println("* NodeTree: Constructor()");

		NodeTree nt = new NodeTree(malformedArr);
	}

}
