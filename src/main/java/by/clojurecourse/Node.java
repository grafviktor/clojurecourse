package by.clojurecourse;

import java.util.List;

public class Node {
	/**
	 * @deprecated Exposing fields.<br>
	 *		Use <b>ru.me.forfun.nodesapp.Node</b>'s<br>
	 *		void addChild(Node node)<br>
	 *		List<Node> getChildren() instead.
	 */
	@Deprecated
	public List<Node> childList;

	/**
	 * @deprecated Exposing fields.<br>
	 *		Use <b>ru.me.forfun.nodesapp.Node</b>'s<br>
	 *		void addInteger(int n)<br>
	 *		List<Integer> getIntegers() instead.
	 */
	@Deprecated
	public List<Integer> numberList;
}
