package ru.me.forfun.nodesapp;

public class App {

	public static void main(String[] args) {
		// Input: [1 [2 3] 4 [5 [6 7]] [8]]
		String[] treeStr = {"[", "1", "[", "2", "3", "]", "4", "[", "5",
			"[", "6", "7", "]", "]", "[", "8", "]", "]"};
		
		NodeTree nt = new NodeTree(treeStr);
		Node root = nt.getRoot();

		printTree(root);
	}

	/* -------------------- Task 2 -------------------- */
	private static void printTree(Node root) {
		int sum = 0;
		for (int i : root.getIntegers()) {
			sum += i;
		}
		System.out.printf("%s, sum: %d\n", root, sum);

		for (Node node : root.getChildren()) {
			printTree(node);
		}
	}
}
