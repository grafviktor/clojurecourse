package ru.me.forfun.nodesapp;

import java.util.ArrayList;
import java.util.List;

public class Node extends by.clojurecourse.Node {

	private String name;
	private List<Node> childList;
	private List<Integer> numberList;

	public Node(String name) {
		childList = new ArrayList<>();
		numberList = new ArrayList<>();
		this.name = name;
	}

	public void addChild(Node node) {
		childList.add(node);
	}

	public void addInteger(int n) {
		numberList.add(n);
	}

	public List<Node> getChildren() {
		return childList;
	}

	public List<Integer> getIntegers() {
		return numberList;
	}

	@Override
	public String toString() {
		return "Node: " + name;
	}
}
