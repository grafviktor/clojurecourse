package ru.me.forfun.nodesapp;

import java.util.Deque;
import java.util.LinkedList;

public class NodeTree {


	/* -------------------- Task 1 -------------------- */
	private Node root;

	private class Token {

		public static final char L_BRACKET = '[';
		public static final char R_BRACKET = ']';
	}

	public NodeTree(String[] strArr) {
		checkInput(strArr);
		char[] arr = strArrToCharArr(strArr);
		root = buildTree(arr);
	}

	private Node buildTree(char[] arr) {
		Node parent = new Node("ROOT");
		Deque<Node> stack = new LinkedList<>();
		for (char c : arr) {
			switch (c) {
				case Token.L_BRACKET:
					stack.push(parent);
					break;
				case Token.R_BRACKET:
					parent = stack.pop();
					break;
				default:
					int n = charToDigit(c);

					Node newNode = new Node("" + n);
					parent = stack.peek();
					parent.addChild(newNode);
					parent.addInteger(n);
					parent = newNode;
			}
		}
		return parent;
	}

	private int charToDigit(char c) {
		int offset = 0x30;
		return c - offset;
	}

	private char[] strArrToCharArr(String[] treeStr) {
		char[] arr = new char[treeStr.length];
		for (int i = 0; i < treeStr.length; i++) {
			arr[i] = treeStr[i].charAt(0);
		}
		return arr;
	}

	/* ---------------End Of Task 1 ------------------- */
	/* -------------------- utils -------------------- */
	public Node getRoot() {
		return root;
	}

	private void checkInput(String[] strArr) {

		if (!strArr[0].equals("[")
			|| !strArr[strArr.length - 1].equals("]")) {
			badArgument();
		}

		for (String s : strArr) {
			if (s.length() > 1) {
				badArgument();
			}
		}

		Deque<Character> stack = new LinkedList<>();
		char[] charArr = strArrToCharArr(strArr);
		for (char c : charArr) {
			if (c == Token.L_BRACKET) {
				stack.push(c);
				continue;
			}

			if (c == Token.R_BRACKET) {
				if (stack.isEmpty()) {
					badArgument();
				}
				stack.pop();
				continue;
			}

			if (!Character.isDigit(c)) {
				badArgument();
			}
		}

		if (!stack.isEmpty()) {
			badArgument();
		}
	}

	private void badArgument() {
		throw new ExceptionInInitializerError(this.getClass().getName() + " Constructor error: malformed argument.");
	}
}
